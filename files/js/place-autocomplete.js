function nearMeLocation() {
    console.log("inside near me");
    if ($('#near-me').is(":checked")) {
        console.log("near me checked");
        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(function (position) {

                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                $('#latitude').val(lat);
                $('#longitude').val(lng);
                console.log(lat + " " + lng);
            });
        }
    }
}
function placeAutocompleteInit() {

    var input = document.getElementById('pac-input');


    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);


    var infowindowContent = document.getElementById('infowindow-content');


    autocomplete.addListener('place_changed', function () {
        if ($('#near_me').is(":checked")) {
            return;
        }
        var place = autocomplete.getPlace();

        if (!place.geometry) {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function (position) {

                    var lat = position.coords.latitude;
                    var lng = position.coords.longitude;
                    $('#latitude').val(lat);
                    $('#longitude').val(lng);
                    console.log(lat + " " + lng);
                });
            }
            return;
        }
        else {
            var location = place.geometry.location;
            var lat = location.lat();
            var lng = location.lng();
            $('#latitude').val(lat);
            $('#longitude').val(lng);
            console.log(location.lat() + " " + location.lng());
        }


        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
    });


}