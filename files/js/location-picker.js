

function initMap() {
    var map, marker;
    $("#location-modal").modal();

    var currentLat = $("#latitude").val();
    var currentLng = $("#longitude").val();
    var location = new google.maps.LatLng(currentLat, currentLng);
    var mapProp = {
        center: location,
        zoom: 50,
        gestureHandling: 'greedy'
    };
    map = new google.maps.Map(document.getElementById('map'), mapProp);
    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: location
    });

    geocodePosition(marker.getPosition());

    google.maps.event.addListener(marker, 'dragend', function () {
        map.setCenter(marker.getPosition());
        geocodePosition(marker.getPosition());
        $("#latitude").val(marker.getPosition().lat());

        $("#longitude").val(marker.getPosition().lng());
    });

    google.maps.event.addListener(map, 'click', function(event) {
        
        marker.setPosition(event.latLng);
        map.setCenter(marker.getPosition());
        geocodePosition(marker.getPosition());
        $("#latitude").val(marker.getPosition().lat());

        $("#longitude").val(marker.getPosition().lng());
    });

  

    if (navigator.geolocation) {

        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            $("#latitude").val(pos.lat);

            $("#longitude").val(pos.lng);


            marker.setPosition(pos);

            map.setCenter(marker.getPosition());
            geocodePosition(marker.getPosition());
        });
    }



}


function geocodePosition(pos) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode
            ({
                latLng: pos
            },
                    function (results, status)
                    {
                        if (status == google.maps.GeocoderStatus.OK)
                        {
                            $("#address-label").html(results[0].formatted_address);
                            $("#address").val(results[0].formatted_address);

                        } else
                        {
                            $("#address-label").html('Cannot determine address at this location.');
                        }
                    }
            );
}

function showMap(){
    var map, marker;
    var currentLat = $("#latitude").val();
    var currentLng = $("#longitude").val();
    var location = new google.maps.LatLng(currentLat, currentLng);
    var mapProp = {
        center: location,
        zoom: 30
    };
    map = new google.maps.Map(document.getElementById('location-map'), mapProp);
    marker = new google.maps.Marker({
        map: map,
        position: location
    });
}