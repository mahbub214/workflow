
$(document).ready( function() {
/*<![CDATA[*/

    var categoryList = /*[[${categoryList}]]*/ [{name:'Restaurant'}];
    
    var categories = []
    for (i = 0; i < categoryList.length; i++) {
        categories[i] = categoryList[i].name;
    }

        $( "#autocomplete-list" ).autocomplete({
            minLength: 0,
        source: categories
        }).focus(function () {
            $(this).autocomplete("search", "");
        });

/*]]>*/
} );
