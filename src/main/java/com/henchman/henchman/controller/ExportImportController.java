/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman.controller;

import com.henchman.henchman.Exporter;
import com.henchman.henchman.Importer;
import com.henchman.henchman.bulk.Bulk;
import com.henchman.henchman.bulk.BulkRepository;
import com.henchman.henchman.mixers.MixerData;
import com.henchman.henchman.mixers.MixerDataRepository;

import com.henchman.henchman.service.UserService;
import com.henchman.henchman.storage.StorageFileNotFoundException;
import com.henchman.henchman.storage.StorageService;
import com.henchman.henchman.weeklyplan.WeeklyPlan;
import com.henchman.henchman.weeklyplan.WeeklyPlanRepository;
import com.henchman.henchman.wup.WUP;
import com.henchman.henchman.wup.WUPRepository;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Mi Notebook Pro
 */
@Controller
public class ExportImportController {

    private final StorageService storageService;

    @Autowired
    WeeklyPlanRepository weeklyPlanRepository;
    
    @Autowired
    BulkRepository bulkRepository;
    
    @Autowired
    MixerDataRepository mixerDataRepository;
    
    @Autowired
    WUPRepository wupRepository;

    @Autowired
    UserService userService;

    @Autowired
    public ExportImportController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/files/{dir}/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String dir, @PathVariable String filename, Authentication authentication) {

        Resource file = storageService.loadAsResource(dir+"/"+filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/export-weekly-plans/{machineNo}")
    public String exportWeeklyPlans(@PathVariable Integer machineNo, Model model) {

        List<WeeklyPlan> plans = weeklyPlanRepository.findByMachineNo(machineNo);
        Exporter exporter = new Exporter();
        String fileName = "files/exported/weekly-plans_"+System.currentTimeMillis()+".xlsx";
        try {
            exporter.exportWeeklyPlans(plans, fileName);
        } catch (IOException ex) {
            model.addAttribute("failMessage", "Weekly plan export failed");
            return "redirect:/machine/"+machineNo;
        }

        model.addAttribute("successMessage", "Weekly plan exported successfully");
        return "redirect:/" + fileName;
    }

    @PostMapping("/import-weekly-plans/{machineNo}")
    public String importWeeklyPlans(@PathVariable Integer machineNo, @RequestParam("file") MultipartFile file, Model model) {
        
        if (file.isEmpty()) {
            model.addAttribute("failMessage", "Please select file.");
            return "redirect:/machine/"+machineNo;
        }

        
        Importer importer = new Importer();
       

        try {
            List<WeeklyPlan> plans = importer.importWeeklyPlans(file);
            for (WeeklyPlan p : plans) {
                System.out.println("something imported ");
                p.setMachineNo(machineNo);
                weeklyPlanRepository.save(p);
            }
        } catch (IOException ex) {
            Logger.getLogger(ExportImportController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "redirect:/machine/"+machineNo;
    }
    
    @GetMapping("/export-bulks")
    public String exportBulks(Model model) {

        List<Bulk> bulks = bulkRepository.findAll();
        Exporter exporter = new Exporter();
        String fileName = "files/exported/bulks_"+System.currentTimeMillis()+".xlsx";
        try {
            exporter.exportBulks(bulks, fileName);
        } catch (IOException ex) {
            model.addAttribute("failMessage", "Bulk export failed");
            return "redirect:/bulks";
        }

        model.addAttribute("successMessage", "Bulk exported successfully");
        return "redirect:/" + fileName;
    }

    @PostMapping("/import-bulks")
    public String importBulks(@RequestParam("file") MultipartFile file, Model model) {
        
        if (file.isEmpty()) {
            model.addAttribute("failMessage", "Please select file.");
            return "redirect:/bulks";
        }

        
        Importer importer = new Importer();
       

        try {
            List<Bulk> bulks = importer.importBulks(file);
            for (Bulk b : bulks) {
                System.out.println("something imported ");
                bulkRepository.save(b);
            }
        } catch (IOException ex) {
            Logger.getLogger(ExportImportController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "redirect:/bulks";
    }

    @GetMapping("/export-wups")
    public String exportWUPs(Model model) {

        List<WUP> wups = wupRepository.findAll();
        Exporter exporter = new Exporter();
        String fileName = "files/exported/wups_"+System.currentTimeMillis()+".xlsx";
        try {
            exporter.exportWUPs(wups, fileName);
        } catch (IOException ex) {
            model.addAttribute("failMessage", "W/Up export failed");
            return "redirect:/wups";
        }

        model.addAttribute("successMessage", "W/Up exported successfully");
        return "redirect:/" + fileName;
    }

    @PostMapping("/import-wups")
    public String importWUPs(@RequestParam("file") MultipartFile file, Model model) {
        
        if (file.isEmpty()) {
            model.addAttribute("failMessage", "Please select file.");
            return "redirect:/wups";
        }

        
        Importer importer = new Importer();
       

        try {
            List<WUP> wups = importer.importWUPs(file);
            for (WUP b : wups) {
                System.out.println("something imported ");
                wupRepository.save(b);
            }
        } catch (IOException ex) {
            Logger.getLogger(ExportImportController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "redirect:/wups";
    }
    
    @GetMapping("/export-mixer-data/{mixerNo}")
    public String exportMixerData(@PathVariable Integer mixerNo, Model model) {

        List<MixerData> mixerDatas = mixerDataRepository.findByMixerNo(mixerNo);
        Exporter exporter = new Exporter();
        String fileName = "files/exported/mixer_data_"+System.currentTimeMillis()+".xlsx";
        try {
            exporter.exportMixerDatas(mixerDatas, fileName);
        } catch (IOException ex) {
            model.addAttribute("failMessage", "Mixer data export failed");
            return "redirect:/mixer/"+mixerNo;
        }

        model.addAttribute("successMessage", "Mixer data exported successfully");
        return "redirect:/" + fileName;
    }

    @PostMapping("/import-mixer-data/{mixerNo}")
    public String importMixerData(@PathVariable Integer mixerNo, @RequestParam("file") MultipartFile file, Model model) {
        System.out.println("inside import mixer file");
        if (file.isEmpty()) {
            model.addAttribute("failMessage", "Please select file.");
            return "redirect:/mixer/"+mixerNo;
        }

        
        Importer importer = new Importer();
       

        try {
            List<MixerData> dataList = importer.importMixerDatas(file);
            for (MixerData data : dataList) {
                data.setMixerNo(mixerNo);
                System.out.println("something imported ");
                mixerDataRepository.save(data);
            }
        } catch (IOException ex) {
            Logger.getLogger(ExportImportController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "redirect:/mixer/"+mixerNo;
    }
    
    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }
}
