package com.henchman.henchman.controller;

import com.henchman.henchman.users.WebUser;
import com.henchman.henchman.repository.RoleRepository;
import com.henchman.henchman.service.UserService;
import com.henchman.henchman.users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/superadmin")
public class SuperAdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/users/new", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("user", new WebUser());
        model.addAttribute("actionurl", "/superadmin/users/new");
        model.addAttribute("roleList", roleRepository.findByRoleNot("SUPERADMIN"));
        return "registration";
    }

    @RequestMapping(value = "/users/new", method = RequestMethod.POST)
    public String createNewUser(@Valid WebUser user, BindingResult bindingResult, Model model, Authentication authentication, RedirectAttributes redir) {
        //ModelAndView modelAndView = new ModelAndView();
        WebUser userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("roleList", roleRepository.findAll());
            model.addAttribute("actionurl", "/superadmin/users/new");
            return "registration";
            //modelAndView.setViewName("registration");
        } else {
            user.setActive(1);

            userService.saveUserWithEncryptedPassword(user);
            redir.addFlashAttribute("successMessage", "User added successfully.");

            return "redirect:/superadmin/users";
        }

    }

    @GetMapping("/users/edit/{userID}")
    public String getUserEditForm(Model model, @PathVariable Integer userID) {
        Optional<WebUser> userOptional = userRepository.findById(userID);
        model.addAttribute("user", userOptional.get());
        model.addAttribute("roleList", roleRepository.findAll());
        model.addAttribute("actionurl", "/superadmin/users/edit/" + userID);
        return "registration";
    }

    @PostMapping("/users/edit/{userID}")
    public String editUser(@Valid WebUser user, BindingResult bindingResult, Model model, @PathVariable Integer userID, RedirectAttributes redir) {
        //ModelAndView modelAndView = new ModelAndView();
        WebUser userExists = userRepository.findByEmail(user.getEmail());
        if (userExists != null && userExists.getId() != userID) {
            bindingResult.rejectValue("email", "error.email",
                    "User already exists with this email.");
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("actionurl", "/superadmin/users/edit/" + userID);
            model.addAttribute("roleList", roleRepository.findAll());
            return "registration";
            //modelAndView.setViewName("registration");
        } else {

            userService.saveUserWithEncryptedPassword(user);
            redir.addFlashAttribute("successMessage", "User edited successfully.");

            return "redirect:/superadmin/users";
        }

    }

    @GetMapping("/users/delete/{userID}")
    public String deleteUser(@PathVariable Integer userID) {
        userRepository.deleteById(userID);
        return "redirect:/superadmin/users";
    }

    @GetMapping("/users/delete/all")
    public String deleteAllUser() {
        userRepository.deleteAll();
        return "redirect:/superadmin/users";
    }

}
