package com.henchman.henchman.controller;

import com.henchman.henchman.mixers.MixerInfo;
import com.henchman.henchman.mixers.MixerInfoRepository;
import com.henchman.henchman.model.Role;
import com.henchman.henchman.users.WebUser;
import com.henchman.henchman.repository.RoleRepository;
import com.henchman.henchman.service.UserService;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    MixerInfoRepository mixerInfoRepository;

    @GetMapping("/setup")
    public String init() {

        Role role = new Role();
        role.setRole("ADMIN");
        roleRepository.save(role);
        role = new Role();
        role.setRole("SUPERVISOR");
        roleRepository.save(role);
        role = new Role();
        role.setRole("SUPERADMIN");
        roleRepository.save(role);

        WebUser user = new WebUser();
        user.setEmail("superadmin@email.com");
        user.setPassword("asdfgh");

        user.setPhoneNumber("1234");
        user.setLastName("Mahbub");
        user.setFirstName("Alam");

        user.setActive(1);
        Role userRole = roleRepository.findByRole("SUPERADMIN");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        userService.saveUserWithEncryptedPassword(user);

        MixerInfo mixerInfo = new MixerInfo();
        mixerInfo.setMixerNo(1);
        mixerInfoRepository.save(mixerInfo);
        
        mixerInfo = new MixerInfo();
        mixerInfo.setMixerNo(2);
        mixerInfoRepository.save(mixerInfo);
        
        mixerInfo = new MixerInfo();
        mixerInfo.setMixerNo(3);
        mixerInfoRepository.save(mixerInfo);
        
        mixerInfo = new MixerInfo();
        mixerInfo.setMixerNo(4);
        mixerInfoRepository.save(mixerInfo);

        return "login";
    }

    @GetMapping(value = {"/", "/login"})
    public String login(Authentication authentication) {

        if (authentication != null && authentication.isAuthenticated()) {
            return "redirect:/login-success";
        }
        return "login";
    }

    @GetMapping("/change-password")
    public String changePasswordGet(Model model, Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        WebUser webuser = userService.findUserByEmail(user.getUsername());
        model.addAttribute("user", webuser);
        return "change-password";

    }

    @PostMapping("/change-password")
    public String changePasswordPost(WebUser changedUser, Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        WebUser webuser = userService.findUserByEmail(user.getUsername());
        webuser.setPassword(changedUser.getPassword());
        userService.saveUserWithEncryptedPassword(webuser);
        return "redirect:/login-success";

    }

    @GetMapping("/login-success")
    public String loginSuccess(Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        WebUser webuser = userService.findUserByEmail(user.getUsername());
        Set<Role> roleSet = webuser.getRoles();
        List<Role> roleList = new ArrayList<>();
        roleList.addAll(roleSet);
        Role role = roleList.get(0);
        if (role.getRole().equals("SUPERADMIN")) {
            return "redirect:/superadmin/users";
        } else {
            return "redirect:/superadmin/users";
        }

    }

}
