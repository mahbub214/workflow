/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman;


import com.henchman.henchman.service.BeanUtil;
import com.henchman.henchman.status.BulkStatus;
import com.henchman.henchman.status.BulkStatusRepository;
import com.henchman.henchman.status.MixerDataStatus;
import com.henchman.henchman.status.MixerDataStatusRepository;
import com.henchman.henchman.status.WUPStatus;
import com.henchman.henchman.status.WUPStatusRepository;
import com.henchman.henchman.status.WeeklyPlanStatus;
import com.henchman.henchman.status.WeeklyPlanStatusRepository;
import com.pusher.rest.Pusher;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Mi Notebook Pro
 */
public class AutoUpdater {
    
    WeeklyPlanStatusRepository weeklyPlanStatusRepository;
    BulkStatusRepository bulkStatusRepository;
    WUPStatusRepository wupStatusRepository;
    MixerDataStatusRepository mixerDataStatusRepository;

    private static AutoUpdater instance = null;
    private Pusher pusher;
    private AutoUpdater(){
  
        pusher = new Pusher("664522", "119f74a0f57201a9ffda", "e149816562842d6dcd60");
        pusher.setCluster("ap2");
        pusher.setEncrypted(false);
        weeklyPlanStatusRepository = BeanUtil.getBean(WeeklyPlanStatusRepository.class);
        bulkStatusRepository = BeanUtil.getBean(BulkStatusRepository.class);
        wupStatusRepository = BeanUtil.getBean(WUPStatusRepository.class);
        mixerDataStatusRepository = BeanUtil.getBean(MixerDataStatusRepository.class);

    }
  
    
    public synchronized static AutoUpdater getInstance(){
        
        if(instance == null)
        {
            instance = new AutoUpdater();
        }
        return instance;
    }


    public void updateWeeklyPlanStatus(String processOrder, String status) {
//        WeeklyPlanStatus planStatus = weeklyPlanStatusRepository.findByProcessOrder(processOrder);
//        if(planStatus == null){
//            planStatus = new WeeklyPlanStatus(processOrder, status);
//        }
//        else{
//            planStatus.setStatus(status);
//        }
        WeeklyPlanStatus planStatus = new WeeklyPlanStatus(processOrder, status);
        weeklyPlanStatusRepository.save(planStatus);
        pusher.trigger("weekly-plan", "status",planStatus);
    }

    public void updateMixerDataStatus(Integer mixerNo, String processOrder, String s, String group) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        MixerDataStatus status = mixerDataStatusRepository.findByMixerNoAndProcessOrderAndStatusStartsWith(mixerNo, processOrder, group);
        if(status == null){
            status = new MixerDataStatus(mixerNo, processOrder, s);
        }
        else{
            status.setStatus(s);
        }
      //  MixerDataStatus status = new MixerDataStatus(mixerNo, processOrder, s);
        mixerDataStatusRepository.save(status);
        pusher.trigger("mixer-data", "status",status);
    }

    public void updateBulkStatus(String processOrder, String s) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        BulkStatus status = bulkStatusRepository.findByProcessOrder(processOrder);
        if(status == null){
            status = new BulkStatus(processOrder, s);
        }
        else{
            status.setStatus(s);
        }
//        BulkStatus status = new BulkStatus(processOrder, s);
        bulkStatusRepository.save(status);
        pusher.trigger("bulk", "status",status);
    }

    public void updateWUPStatus(String processOrder, String s) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        WUPStatus status = wupStatusRepository.findByProcessOrder(processOrder);
        if(status == null){
            status = new WUPStatus(processOrder, s);
        }
        else{
            status.setStatus(s);
        }
//        WUPStatus status = new WUPStatus(processOrder, s);
        wupStatusRepository.save(status);
        pusher.trigger("wup", "status",status);
    }

   

}
