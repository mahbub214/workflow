/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman.users;

import com.henchman.henchman.service.UserService;
import com.henchman.henchman.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author Mi Notebook Pro
 */
@Controller
public class UserController {

    private final StorageService storageService;

    @Autowired
    public UserController(StorageService storageService) {
        this.storageService = storageService;
    }

    @Autowired
    UserRepository userRepository;
    
     @Autowired
    UserService userService;

    @GetMapping(value = "/superadmin/users")
    public String list(Model model) {

        model.addAttribute("userList", userRepository.findAll());

        return "users";

    }

//    @GetMapping("/superadmin/users/new")
//    public String getUserAddForm(Model model) {
//        model.addAttribute("user", new WebUser());
//        model.addAttribute("actionurl", "/superadmin/users/new");
//        return "user-form";
//    }
//
//    @PostMapping("/superadmin/users/new")
//    public String createNewUser(@Valid WebUser user, BindingResult bindingResult, Model model, RedirectAttributes redir) {
//        //ModelAndView modelAndView = new ModelAndView();
//        if (userRepository.existsByEmail(user.getEmail())) {
//            bindingResult.rejectValue("email", "error.email",
//                    "User exists with this email");
//        }
//
//        if (bindingResult.hasErrors()) {
//            model.addAttribute("user", user);
//            model.addAttribute("actionurl", "/superadmin/users/new");
//            return "user-form";
//            //modelAndView.setViewName("registration");
//        } else {
//            
////            userRepository.save(user);
//            userService.saveUserWithEncryptedPassword(user);
//            redir.addFlashAttribute("successMessage", "User added successfully.");
//
//            return "redirect:/superadmin/users";
//        }
//
//    }
//
//    @GetMapping("/superadmin/users/edit/{userID}")
//    public String getUserEditForm(Model model, @PathVariable Integer userID) {
//        Optional<WebUser> userOptional = userRepository.findById(userID);
//        model.addAttribute("user", userOptional.get());
//        model.addAttribute("actionurl", "/superadmin/users/edit/" + userID);
//        return "user-form";
//    }
//
//    @PostMapping("/superadmin/users/edit/{userID}")
//    public String editUser(@Valid WebUser user, BindingResult bindingResult, Model model, @PathVariable Integer userID, RedirectAttributes redir) {
//        //ModelAndView modelAndView = new ModelAndView();
//        WebUser userExists = userRepository.findByEmail(user.getEmail());
//        if (userExists != null && userExists.getId() != userID) {
//            bindingResult.rejectValue("email", "error.email",
//                    "User already exists with this email.");
//        }
//
//        if (bindingResult.hasErrors()) {
//            model.addAttribute("user", user);
//            model.addAttribute("actionurl", "/superadmin/users/edit/" + userID);
//            return "user-form";
//            //modelAndView.setViewName("registration");
//        } else {
//
//            userService.saveUserWithEncryptedPassword(user);
//            redir.addFlashAttribute("successMessage", "User edited successfully.");
//
//            return "redirect:/superadmin/users";
//        }
//
//    }
//
//    @GetMapping("/superadmin/users/delete/{userID}")
//    public String deleteUser(@PathVariable Integer userID) {
//        userRepository.deleteById(userID);
//        return "redirect:/superadmin/users";
//    }
//
//    @GetMapping("/superadmin/users/delete/all")
//    public String deleteAllUser() {
//        userRepository.deleteAll();
//        return "redirect:/superadmin/users";
//    }
}
