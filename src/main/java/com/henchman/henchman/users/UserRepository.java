package com.henchman.henchman.users;




import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("userRepository")
public interface UserRepository extends JpaRepository<WebUser, Integer> {
	 WebUser findByEmail(String email);

    public boolean existsByEmail(String email);
}
