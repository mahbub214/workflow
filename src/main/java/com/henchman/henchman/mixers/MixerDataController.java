package com.henchman.henchman.mixers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MixerDataController {

    @Autowired
    private MixerDataRepository mixerDataRepository;

    @GetMapping("/mixer-data/new/{mixerNo}")
    public String getMixerDataForm(@PathVariable Integer mixerNo, Model model) {
        MixerData data = new MixerData();
        model.addAttribute("mixerData", data);
        model.addAttribute("mixerNo", mixerNo);
        model.addAttribute("actionurl", "/mixer-data/new/"+mixerNo);
        return "mixer-form";
    }

    @PostMapping("/mixer-data/new/{mixerNo}")
    public String createMixerData(@PathVariable Integer mixerNo, @Valid MixerData mixerData, BindingResult bindingResult, Model model, RedirectAttributes redir) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("mixerData", mixerData);
            model.addAttribute("mixerNo", mixerNo);
            model.addAttribute("actionurl", "/mixer-data/new/"+mixerNo);
            return "mixer-form";
        } else {

            mixerData.setMixerNo(mixerNo);
            mixerDataRepository.save(mixerData);
            redir.addFlashAttribute("successMessage", "Mixer data added successfully.");

            return "redirect:/mixer/"+mixerNo;
        }
    }

}
