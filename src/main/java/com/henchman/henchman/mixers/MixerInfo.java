/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman.mixers;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Mi Notebook Pro
 */
@Entity
@Table(name = "mixer_info")
public class MixerInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(name="mixer_no")
    private Integer mixerNo;
    
    @Column(name = "day_operator")
    private String dayOperator;
    
    @Column(name = "arvo_operator")
    private String arvoOperator;
    
    @Column(name = "night_operator")
    private String nightOperator;
    
    @Column(name = "score_5s")
    private String score5s;
    
    @Column(name = "score_oee")
    private String scoreOEE;
     
    @Column(name = "logo")
    private String logo;
    
    @Column(name="sort1")
    private boolean sort1;
    
    @Column(name="sort2")
    private boolean sort2;
    
    @Column(name="sort3")
    private boolean sort3;
    
    @Column(name="set1")
    private boolean set1;
    
    @Column(name="set2")
    private boolean set2;
        
    @Column(name="set3")
    private boolean set3;
    
    @Column(name="shine1")
    private boolean shine1;
    
    @Column(name="shine2")
    private boolean shine2;
     
    @Column(name="shine3")
    private boolean shine3;
    
    @Column(name="standard1")
    private boolean standard1;
    
    @Column(name="standard2")
    private boolean standard2;
    
    @Column(name="standard3")
    private boolean standard3;
    
    @Column(name="sustain1")
    private boolean sustain1;
    
    @Column(name="sustain2")
    private boolean sustain2;
    
    @Column(name="sustain3")
    private boolean sustain3;
    
    @Column(name="stopwatch")
    private String stopwatch="";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getMixerNo() {
        return mixerNo;
    }

    public void setMixerNo(Integer mixerNo) {
        this.mixerNo = mixerNo;
    }

    public String getDayOperator() {
        return dayOperator;
    }

    public void setDayOperator(String dayOperator) {
        this.dayOperator = dayOperator;
    }

    public String getArvoOperator() {
        return arvoOperator;
    }

    public void setArvoOperator(String arvoOperator) {
        this.arvoOperator = arvoOperator;
    }

    public String getNightOperator() {
        return nightOperator;
    }

    public void setNightOperator(String nightOperator) {
        this.nightOperator = nightOperator;
    }

    public String getScore5s() {
        return score5s;
    }

    public void setScore5s(String score5s) {
        this.score5s = score5s;
    }

    public String getScoreOEE() {
        return scoreOEE;
    }

    public void setScoreOEE(String scoreOEE) {
        this.scoreOEE = scoreOEE;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean isSort1() {
        return sort1;
    }

    public void setSort1(boolean sort1) {
        this.sort1 = sort1;
    }

    public boolean isSort2() {
        return sort2;
    }

    public void setSort2(boolean sort2) {
        this.sort2 = sort2;
    }

    public boolean isSort3() {
        return sort3;
    }

    public void setSort3(boolean sort3) {
        this.sort3 = sort3;
    }

    public boolean isSet1() {
        return set1;
    }

    public void setSet1(boolean set1) {
        this.set1 = set1;
    }

    public boolean isSet2() {
        return set2;
    }

    public void setSet2(boolean set2) {
        this.set2 = set2;
    }

    public boolean isSet3() {
        return set3;
    }

    public void setSet3(boolean set3) {
        this.set3 = set3;
    }

    public boolean isShine1() {
        return shine1;
    }

    public void setShine1(boolean shine1) {
        this.shine1 = shine1;
    }

    public boolean isShine2() {
        return shine2;
    }

    public void setShine2(boolean shine2) {
        this.shine2 = shine2;
    }

    public boolean isShine3() {
        return shine3;
    }

    public void setShine3(boolean shine3) {
        this.shine3 = shine3;
    }

    public boolean isStandard1() {
        return standard1;
    }

    public void setStandard1(boolean standard1) {
        this.standard1 = standard1;
    }

    public boolean isStandard2() {
        return standard2;
    }

    public void setStandard2(boolean standard2) {
        this.standard2 = standard2;
    }

    public boolean isStandard3() {
        return standard3;
    }

    public void setStandard3(boolean standard3) {
        this.standard3 = standard3;
    }

    public boolean isSustain1() {
        return sustain1;
    }

    public void setSustain1(boolean sustain1) {
        this.sustain1 = sustain1;
    }

    public boolean isSustain2() {
        return sustain2;
    }

    public void setSustain2(boolean sustain2) {
        this.sustain2 = sustain2;
    }

    public boolean isSustain3() {
        return sustain3;
    }

    public void setSustain3(boolean sustain3) {
        this.sustain3 = sustain3;
    }

    public String getStopwatch() {
        return stopwatch;
    }

    public void setStopwatch(String stopwatch) {
        this.stopwatch = stopwatch;
    }
    
 
    
    
}
