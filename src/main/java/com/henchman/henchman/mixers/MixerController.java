/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman.mixers;

import com.henchman.henchman.storage.StorageService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Mi Notebook Pro
 */
@Controller
public class MixerController {

    @Autowired
    MixerInfoRepository mixerInfoRepository;

    @Autowired
    MixerDataRepository mixerDataRepository;

    private final StorageService storageService;

    @Autowired
    public MixerController(StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping(value = "/mixer/{mixerNo}")
    public String list(@PathVariable Integer mixerNo, Model model) {

        model.addAttribute("mixer", mixerInfoRepository.findByMixerNo(mixerNo));
        model.addAttribute("mixerDataList", mixerDataRepository.findByMixerNo(mixerNo));
        model.addAttribute("mixerNo", mixerNo);

        return "mixer";

    }

    @PostMapping(value = "/mixer/{mixerNo}")
    public String postMixerInfo(@Valid MixerInfo mixer, BindingResult bindingResult, @RequestParam("file") MultipartFile file, Model model, @PathVariable Integer mixerNo) {
        MixerInfo mixerInfo = mixerInfoRepository.findByMixerNo(mixerNo);
        mixer.setId(mixerInfo.getId());
        if (bindingResult.hasErrors()) {

            model.addAttribute("mixer", mixer);
            model.addAttribute("mixerNo", mixerNo);

            return "mixer";
        } else {

            if (!file.isEmpty()) {
                storageService.store(file, mixerNo + "/");
                mixer.setLogo("/files/" + mixerNo + "/" + file.getOriginalFilename());
            }

            mixerInfoRepository.save(mixer);
            return "redirect:/mixer/" + mixerNo;
        }
    }

}
