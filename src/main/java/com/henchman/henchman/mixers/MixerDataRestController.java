/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman.mixers;

import com.henchman.henchman.AutoUpdater;
import com.henchman.henchman.processtime.ProcessTime;
import com.henchman.henchman.processtime.ProcessTimeRepository;
import com.henchman.henchman.status.MixerDataStatus;
import com.henchman.henchman.status.MixerDataStatusRepository;
import com.henchman.henchman.status.WeeklyPlanStatus;
import com.henchman.henchman.status.WeeklyPlanStatusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mi Notebook Pro
 */
@RestController
public class MixerDataRestController {

    @Autowired
    MixerDataStatusRepository mixerDataStatusRepository;
    
    @Autowired
    ProcessTimeRepository processTimeRepository;
    
    @GetMapping("/mixer-data/refresh-status/{mixerNo}")
    public List<MixerDataStatus> refreshStatus(@PathVariable Integer mixerNo) {
        List<MixerDataStatus> dataList = mixerDataStatusRepository.findByMixerNo(mixerNo);
        return dataList;
    }

    @GetMapping("/status/done/{mixerNo}/{processOrder}")
    public String statusDone(@PathVariable Integer mixerNo, @PathVariable String processOrder) {
        AutoUpdater.getInstance().updateMixerDataStatus(mixerNo, processOrder, "status_done", "status");
        AutoUpdater.getInstance().updateWeeklyPlanStatus(processOrder, "done");
        return "ok";
    }

    @GetMapping("/status/in/{mixerNo}/{processOrder}")
    public String statusIn(@PathVariable Integer mixerNo, @PathVariable String processOrder) {
        AutoUpdater.getInstance().updateMixerDataStatus(mixerNo, processOrder, "status_in", "status");
        ProcessTime pt = processTimeRepository.findByProcessOrder(processOrder);
        if(pt == null){
            pt = new ProcessTime();
            pt.setProcessOrder(processOrder);
        }
        pt.setStartTime(System.currentTimeMillis());
        processTimeRepository.save(pt);
        return "ok";
    }

    @GetMapping("/status/out/{mixerNo}/{processOrder}")
    public String statusOut(@PathVariable Integer mixerNo, @PathVariable String processOrder) {
        AutoUpdater.getInstance().updateMixerDataStatus(mixerNo, processOrder, "status_out", "status");
        ProcessTime pt = processTimeRepository.findByProcessOrder(processOrder);
        if(pt != null){
            pt.setEndTime(System.currentTimeMillis());
            double interval = (pt.getEndTime()-pt.getStartTime())/(1000*60);
            pt.setProcessTime(interval);
            processTimeRepository.save(pt);
        }
        return "ok";
    }
    
    @GetMapping("/target/{status}/{mixerNo}/{processOrder}")
    public String targetStatus(@PathVariable String status, @PathVariable Integer mixerNo, @PathVariable String processOrder) {
        AutoUpdater.getInstance().updateMixerDataStatus(mixerNo, processOrder, "target_"+status, "target");
        return "ok";
    }
    
    @GetMapping("/ipc/{status}/{mixerNo}/{processOrder}")
    public String ipcStatus(@PathVariable String status, @PathVariable Integer mixerNo, @PathVariable String processOrder) {
        AutoUpdater.getInstance().updateMixerDataStatus(mixerNo, processOrder, "ipc_"+status, "ipc");
        return "ok";
    }
    
    @GetMapping("/changeover/{status}/{mixerNo}/{processOrder}")
    public String changeOverStatus(@PathVariable String status, @PathVariable Integer mixerNo, @PathVariable String processOrder) {
        AutoUpdater.getInstance().updateMixerDataStatus(mixerNo, processOrder, "change_over_"+status, "change_over");
        return "ok";
    }
}
