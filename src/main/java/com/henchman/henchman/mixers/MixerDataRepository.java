package com.henchman.henchman.mixers;





import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("mixerDataRepository")
public interface MixerDataRepository extends JpaRepository<MixerData, Integer> {

    public List<MixerData> findByMixerNo(Integer mixerNo);


}
