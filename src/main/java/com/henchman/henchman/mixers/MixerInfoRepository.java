package com.henchman.henchman.mixers;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("mixerInfoRepository")
public interface MixerInfoRepository extends JpaRepository<MixerInfo, Integer> {

    public MixerInfo findByMixerNo(Integer mixerNo);

}
