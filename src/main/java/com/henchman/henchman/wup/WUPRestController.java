/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman.wup;

import com.henchman.henchman.AutoUpdater;
import com.henchman.henchman.status.WUPStatus;
import com.henchman.henchman.status.WUPStatusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mi Notebook Pro
 */
@RestController
public class WUPRestController {
    
        @Autowired
        WUPStatusRepository wupStatusRepository;
        
        @GetMapping("/wup/refresh-status")
        public List<WUPStatus> refreshStatus()
        {
            List<WUPStatus> dataList = wupStatusRepository.findAll();
            return dataList;
        }
        @GetMapping("/wup/start/{processOrder}")
        public String wupStart(@PathVariable String processOrder)
        {
                        
            AutoUpdater.getInstance().updateWUPStatus(processOrder, "start");
            return "ok";
        }
        @GetMapping("/wup/finish/{processOrder}")
        public String wupFinish(@PathVariable String processOrder)
        {            
            AutoUpdater.getInstance().updateWUPStatus(processOrder, "finish");

            AutoUpdater.getInstance().updateWeeklyPlanStatus(processOrder, "wup");
            return "ok";
        }
        @GetMapping("/wup/hold/{processOrder}")
        public String wupHold(@PathVariable String processOrder)
        {            
            AutoUpdater.getInstance().updateWUPStatus(processOrder, "hold");

            AutoUpdater.getInstance().updateWeeklyPlanStatus(processOrder, "hold");
            return "ok";
        }
        
}
