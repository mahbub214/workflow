package com.henchman.henchman.wup;

import com.henchman.henchman.bulk.Bulk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class WUPController {

    @Autowired
    private WUPRepository wupRepository;

    @GetMapping("/wups")
        public String getWUPs(Model model)
        {
            List<WUP> wupList = wupRepository.findAll();
            model.addAttribute("wupList", wupList);
            return "wup";
        }

    @GetMapping("/wups/new")
    public String getWUP(Model model) {
        model.addAttribute("wup", new WUP());
        model.addAttribute("actionurl", "/wups/new");
        return "wup-form";
    }
    
    @PostMapping("/wups/new")
    public String createWUP(@Valid WUP wup,  BindingResult bindingResult, Model model, RedirectAttributes redir)
    {
        if (bindingResult.hasErrors()) {
            model.addAttribute("wup", wup);
            model.addAttribute("actionurl", "/wups/new");
            return "wup-form";
        } else {
            
            wupRepository.save(wup);
            redir.addFlashAttribute("successMessage", "WUP added successfully.");

            return "redirect:/wups";
        }
    }

}
