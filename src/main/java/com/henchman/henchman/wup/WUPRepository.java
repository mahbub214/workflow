package com.henchman.henchman.wup;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("wupRepository")
public interface WUPRepository extends JpaRepository<WUP, Integer> {

}
