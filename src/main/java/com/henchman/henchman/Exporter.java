/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman;

import com.henchman.henchman.bulk.Bulk;
import com.henchman.henchman.mixers.MixerData;
import com.henchman.henchman.weeklyplan.WeeklyPlan;
import com.henchman.henchman.wup.WUP;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Mi Notebook Pro
 */
public class Exporter {

    public void exportWeeklyPlans(List<WeeklyPlan> weeklyPlans, String fileName) throws IOException {

        String[] COLUMNs = {"Date", "Mixer No.", "Product Code", "Process Order", "QTY", "Comments"};

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Weekly Plan");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Row for Header
        Row headerRow = sheet.createRow(0);

        // Header
        for (int col = 0; col < COLUMNs.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(COLUMNs[col]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowIdx = 1;
        for (WeeklyPlan weeklyPlan : weeklyPlans) {
            Row row = sheet.createRow(rowIdx++);

            row.createCell(0).setCellValue(weeklyPlan.getDate());
            row.createCell(1).setCellValue(weeklyPlan.getMixerNo());
            row.createCell(2).setCellValue(weeklyPlan.getProductCode());
            row.createCell(3).setCellValue(weeklyPlan.getProcessOrder());
            row.createCell(4).setCellValue(weeklyPlan.getQuantity());
            row.createCell(5).setCellValue(weeklyPlan.getComment());

        }

        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }

    public void exportBulks(List<Bulk> bulks, String fileName) throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String[] COLUMNs = {"Operator", "Product Code", "Process Order", "QTY", "Comments"};

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Bulk");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Row for Header
        Row headerRow = sheet.createRow(0);

        // Header
        for (int col = 0; col < COLUMNs.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(COLUMNs[col]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowIdx = 1;
        for (Bulk bulk : bulks) {
            Row row = sheet.createRow(rowIdx++);

            row.createCell(1).setCellValue(bulk.getOperator());
            row.createCell(2).setCellValue(bulk.getProductCode());
            row.createCell(3).setCellValue(bulk.getProcessOrder());
            row.createCell(4).setCellValue(bulk.getQuantity());
            row.createCell(5).setCellValue(bulk.getComment());

        }

        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }

    public void exportWUPs(List<WUP> wups, String fileName) throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String[] COLUMNs = {"Operator", "Product Code", "Process Order", "QTY", "Comments"};

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Bulk");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Row for Header
        Row headerRow = sheet.createRow(0);

        // Header
        for (int col = 0; col < COLUMNs.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(COLUMNs[col]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowIdx = 1;
        for (WUP wup : wups) {
            Row row = sheet.createRow(rowIdx++);

            row.createCell(1).setCellValue(wup.getOperator());
            row.createCell(2).setCellValue(wup.getProductCode());
            row.createCell(3).setCellValue(wup.getProcessOrder());
            row.createCell(4).setCellValue(wup.getQuantity());
            row.createCell(5).setCellValue(wup.getComment());

        }

        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }

    public void exportMixerDatas(List<MixerData> dataList, String fileName) throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String[] COLUMNs = {"Group Code", "Process Order", "Description", "QTY", "Planned Starting Time", "Planned Finish Time", "Comments"};

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Mixer Data");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Row for Header
        Row headerRow = sheet.createRow(0);

        // Header
        for (int col = 0; col < COLUMNs.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(COLUMNs[col]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowIdx = 1;
        for (MixerData data : dataList) {
            Row row = sheet.createRow(rowIdx++);

            row.createCell(1).setCellValue(data.getGroupCode());
            row.createCell(2).setCellValue(data.getProcessOrder());
            row.createCell(3).setCellValue(data.getDescription());
            row.createCell(4).setCellValue(data.getQuantity());
            row.createCell(5).setCellValue(data.getStartTime());
            row.createCell(6).setCellValue(data.getEndTime());
            row.createCell(7).setCellValue(data.getComment());

        }

        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }
}
