package com.henchman.henchman;

import com.henchman.henchman.storage.StorageProperties;
import com.henchman.henchman.storage.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class HenchManApplication{

    public static void main(String[] args) {
        SpringApplication.run(HenchManApplication.class, args);
//        Wsdl wsdl = Wsdl.parse("http://www.webservicex.net/CurrencyConvertor.asmx?WSDL");
//
//        SoapBuilder builder = wsdl.binding()
//                .localPart("CurrencyConvertorSoap")
//                .find();
//        SoapOperation operation = builder.operation()
//                .soapAction("http://www.webserviceX.NET/ConversionRate")
//                .find();
//        Request request = builder.buildInputMessage(operation)
//    
//        SoapClient client = SoapClient.builder()
//                .endpointUrl("http://www.webservicex.net/CurrencyConvertor.asmx")
//                .build();
//        String response = client.post(request);
    }

    @Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            // storageService.deleteAll();
            storageService.init();
        };
    }
}
