package com.henchman.henchman.bulk;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("bulkRepository")
public interface BulkRepository extends JpaRepository<Bulk, Integer> {

}
