/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman.bulk;

import com.henchman.henchman.AutoUpdater;
import com.henchman.henchman.status.BulkStatus;
import com.henchman.henchman.status.BulkStatusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mi Notebook Pro
 */
@RestController
public class BulkRestController {
    
        @Autowired
        BulkStatusRepository bulkStatusRepository;
        
        @GetMapping("/bulk/refresh-status")
        public List<BulkStatus> refreshStatus()
        {
            List<BulkStatus> dataList = bulkStatusRepository.findAll();
            return dataList;
        }
        
        @GetMapping("/bulk/start/{processOrder}")
        public String bulkStart(@PathVariable String processOrder)
        {
            AutoUpdater.getInstance().updateBulkStatus(processOrder, "start");
            return "ok";
        }
        @GetMapping("/bulk/finish/{processOrder}")
        public String bulkFinish(@PathVariable String processOrder)
        {
                        
            AutoUpdater.getInstance().updateBulkStatus(processOrder, "finish");

            AutoUpdater.getInstance().updateWeeklyPlanStatus(processOrder, "bulk");
            return "ok";
        }
        @GetMapping("/bulk/hold/{processOrder}")
        public String bulkHold(@PathVariable String processOrder)
        {
            AutoUpdater.getInstance().updateBulkStatus(processOrder, "hold");

            AutoUpdater.getInstance().updateWeeklyPlanStatus(processOrder, "hold");
            return "ok";
        }
        
}
