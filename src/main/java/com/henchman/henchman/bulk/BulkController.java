package com.henchman.henchman.controller;

import com.henchman.henchman.bulk.Bulk;
import com.henchman.henchman.bulk.BulkRepository;
import com.henchman.henchman.service.UserService;
import com.henchman.henchman.weeklyplan.WeeklyPlan;
import com.henchman.henchman.weeklyplan.WeeklyPlanRepository;
import com.henchman.henchman.wup.WUP;
import com.henchman.henchman.wup.WUPRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class BulkController {

    @Autowired
    private BulkRepository bulkRepository;

    @GetMapping("/bulks")
    public String getBulks(Model model) {
        List<Bulk> bulkList = bulkRepository.findAll();
        model.addAttribute("bulkList", bulkList);
        return "bulk";
    }

    @GetMapping("/bulks/new")
    public String getBulk(Model model) {
        model.addAttribute("bulk", new Bulk());
        model.addAttribute("actionurl", "/bulks/new");
        return "bulk-form";
    }
    
    @PostMapping("/bulks/new")
    public String createBulk(@Valid Bulk bulk,  BindingResult bindingResult, Model model, RedirectAttributes redir)
    {
        if (bindingResult.hasErrors()) {
            model.addAttribute("bulk", bulk);
            model.addAttribute("actionurl", "/bulks/new");
            return "bulk-form";
        } else {
            
            bulkRepository.save(bulk);
            redir.addFlashAttribute("successMessage", "Bulk added successfully.");

            return "redirect:/bulks";
        }
    }

}
