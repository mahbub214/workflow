package com.henchman.henchman.processtime;

import com.henchman.henchman.bulk.Bulk;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ProcessTimeController {

    @Autowired
    private ProcessTimeRepository processTimeRepository;

    @GetMapping("/dashboard")
        public String getDashboard(Model model)
        {
            List<ProcessTime> ptList = processTimeRepository.findAllByOrderByProcessOrderAsc();
            ArrayList<String> processList = new ArrayList<>();
            ArrayList<Double> timeList = new ArrayList<>();
            if(ptList!=null){
                for(ProcessTime pt: ptList){
                    processList.add(pt.getProcessOrder());
                    timeList.add(pt.getProcessTime());
                }
            }
            model.addAttribute("processList", processList);
            model.addAttribute("timeList", timeList);
            return "admin/dashboard";
        }

    
}
