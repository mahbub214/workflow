package com.henchman.henchman.processtime;





import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("processTimeRepository")
public interface ProcessTimeRepository extends JpaRepository<ProcessTime, Integer> {

    public ProcessTime findByProcessOrderAndEndTime(String processOrder, Long endTime);

    public ProcessTime findByProcessOrder(String processOrder);

    public List<ProcessTime> findAllByOrderByProcessOrderAsc();


}
