package com.henchman.henchman.weeklyplan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class WeeklyPlanController {

    @Autowired
    private WeeklyPlanRepository weeklyPlanRepository;

    @GetMapping("/machine/{machineNo}")
    public String getWeeklyPlans(@PathVariable Integer machineNo, Model model) {
        List<WeeklyPlan> weeklyPlanList = weeklyPlanRepository.findByMachineNo(machineNo);
        model.addAttribute("weeklyPlanList", weeklyPlanList);
        model.addAttribute("machineNo", machineNo);
        return "weekly-plan";
    }

    @GetMapping("/machine/new/{machineNo}")
    public String getWeeklyPlan(@PathVariable Integer machineNo, Model model) {
        model.addAttribute("weeklyPlan", new WeeklyPlan());
        model.addAttribute("machineNo", machineNo);
        model.addAttribute("actionurl", "/machine/new/"+machineNo);
        return "weekly-plan-form";
    }

    @PostMapping("/machine/new/{machineNo}")
    public String createWeeklyPlan(@PathVariable Integer machineNo, @Valid WeeklyPlan weeklyPlan, BindingResult bindingResult, Model model, RedirectAttributes redir) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("weeklyPlan", weeklyPlan);
                    
            model.addAttribute("machineNo", machineNo);

            model.addAttribute("actionurl", "/machine/new/"+machineNo);
            return "weekly-plan-form";
        } else {

            weeklyPlan.setMachineNo(machineNo);
            weeklyPlanRepository.save(weeklyPlan);
            redir.addFlashAttribute("successMessage", "Weekly Plan added successfully.");

            return "redirect:/machine/"+machineNo;
        }
    }

}
