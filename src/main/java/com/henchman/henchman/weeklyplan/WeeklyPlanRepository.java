package com.henchman.henchman.weeklyplan;





import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("weeklyPlanRepository")
public interface WeeklyPlanRepository extends JpaRepository<WeeklyPlan, Integer> {

    public List<WeeklyPlan> findByMachineNo(Integer machineNo);

}
