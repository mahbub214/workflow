/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman.weeklyplan;

import com.henchman.henchman.status.WeeklyPlanStatus;
import com.henchman.henchman.status.WeeklyPlanStatusRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Mi Notebook Pro
 */
@RestController
public class WeeklyPlanRestController {
    
        @Autowired
        WeeklyPlanStatusRepository weeklyPlanStatusRepository;
        
        @GetMapping("/weeklyplan/refresh-status")
        public List<WeeklyPlanStatus> refreshStatus()
        {
            List<WeeklyPlanStatus> dataList = weeklyPlanStatusRepository.findAll();
            return dataList;
        }
        
//        @GetMapping("/hold/{processOrder}")
//        public String hold(@PathVariable String processOrder)
//        {
//            AutoUpdater.getInstance().updateWeeklyPlanStatus(processOrder, "hold");
//            return "ok";
//        }
//        
//        @GetMapping("/done/{processOrder}")
//        public String done(@PathVariable String processOrder)
//        {
//            AutoUpdater.getInstance().updateWeeklyPlanStatus(processOrder, "done");
//            return "ok";
//        }
}
