package com.henchman.henchman.service;

import com.henchman.henchman.users.UserRepository;
import com.henchman.henchman.users.WebUser;
import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;



@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
        @Autowired
        private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public WebUser findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}
        
        @Override
        public void saveUserWithEncryptedPassword(WebUser user)
        {
            		
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            saveUser(user);

        }
	@Override
	public void saveUser(WebUser user) {
       
                userRepository.save(user);
	}

}
