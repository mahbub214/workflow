package com.henchman.henchman.service;

import com.henchman.henchman.users.WebUser;



public interface UserService {
	public WebUser findUserByEmail(String email);
	public void saveUser(WebUser user);
        	
        public void saveUserWithEncryptedPassword(WebUser user);

        
}
