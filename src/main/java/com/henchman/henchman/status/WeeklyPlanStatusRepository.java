package com.henchman.henchman.status;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("weeklyPlanStatusRepository")
public interface WeeklyPlanStatusRepository extends JpaRepository<WeeklyPlanStatus, Integer> {

    public WeeklyPlanStatus findByProcessOrder(String processOrder);
    
}
