package com.henchman.henchman.status;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("wupStatusRepository")
public interface WUPStatusRepository extends JpaRepository<WUPStatus, Integer> {

    public WUPStatus findByProcessOrder(String processOrder);
    
}
