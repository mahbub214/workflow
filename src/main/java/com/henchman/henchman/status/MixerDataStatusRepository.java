package com.henchman.henchman.status;





import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("mixerDataStatusRepository")
public interface MixerDataStatusRepository extends JpaRepository<MixerDataStatus, Integer> {

//    public MixerDataStatus findByMixerNoAndProcessOrder(Integer mixerNo, String processOrder);

    public List<MixerDataStatus> findByMixerNo(Integer mixerNo);

    public MixerDataStatus findByMixerNoAndProcessOrderAndStatusStartsWith(Integer mixerNo, String processOrder, String group);
    
}
