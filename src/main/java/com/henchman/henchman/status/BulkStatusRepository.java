package com.henchman.henchman.status;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("bulkStatusRepository")
public interface BulkStatusRepository extends JpaRepository<BulkStatus, Integer> {

    public BulkStatus findByProcessOrder(String processOrder);
    
}
