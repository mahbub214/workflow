/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.henchman.henchman;

import com.henchman.henchman.bulk.Bulk;
import com.henchman.henchman.mixers.MixerData;
import com.henchman.henchman.weeklyplan.WeeklyPlan;
import com.henchman.henchman.wup.WUP;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Mi Notebook Pro
 */
public class Importer {

    public List<WeeklyPlan> importWeeklyPlans(MultipartFile file) throws IOException {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        InputStream excelFile = new BufferedInputStream(file.getInputStream());
        Workbook workbook = new XSSFWorkbook(excelFile);

        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rows = sheet.iterator();

        List<WeeklyPlan> weeklyPlans = new ArrayList<WeeklyPlan>();
        while (rows.hasNext()) {
            Row row = rows.next();
            if (row.getRowNum() == 0) {
                continue;
            }
            WeeklyPlan p = new WeeklyPlan();
            Cell c = row.getCell(0);
            String date = "";
            if (c != null && c.getCellTypeEnum() == CellType.NUMERIC) {
                DataFormatter formatter = new DataFormatter();
                date = formatter.formatCellValue(c);
            }

            p.setDate(date);

            c = row.getCell(1);
            String mixerNo = "";
            if (c != null) {
                if (c.getCellTypeEnum() == CellType.NUMERIC) {
                    DataFormatter formatter = new DataFormatter();
                    mixerNo = formatter.formatCellValue(c);
                } else {
                    mixerNo = c.getStringCellValue();
                }
//                            mixerNo = c.getStringCellValue();
            }

            p.setMixerNo(mixerNo);

            String productCode = "";
            c = row.getCell(2);
            if (c != null) {
                if (c.getCellTypeEnum() == CellType.NUMERIC) {
                    DataFormatter formatter = new DataFormatter();
                    productCode = formatter.formatCellValue(c);
                } else {
                    productCode = c.getStringCellValue();
                }
            }
            p.setProductCode(productCode);

            c = row.getCell(3);
            String processOrder = "";
            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                processOrder = c.getStringCellValue();
            }
            p.setProcessOrder(processOrder);

            c = row.getCell(4);
            double quantity = 0;
            if (c != null && c.getCellTypeEnum() == CellType.NUMERIC) {
                quantity = c.getNumericCellValue();
            }
            p.setQuantity(quantity);

            String comment = "";
            c = row.getCell(5);

            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                comment = c.getStringCellValue();
            }

            p.setComment(comment);

            weeklyPlans.add(p);
        }

        workbook.close();
        excelFile.close();
        return weeklyPlans;

    }

    public List<Bulk> importBulks(MultipartFile file) throws IOException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        InputStream excelFile = new BufferedInputStream(file.getInputStream());
        Workbook workbook = new XSSFWorkbook(excelFile);

        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rows = sheet.iterator();

        List<Bulk> bulks = new ArrayList<Bulk>();
        while (rows.hasNext()) {
            Row row = rows.next();
            if (row.getRowNum() == 0) {
                continue;
            }
            Bulk p = new Bulk();
            Cell c = row.getCell(0);
            String operator = "";
            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                operator = c.getStringCellValue();
            }

            p.setOperator(operator);

         

            String productCode = "";
            c = row.getCell(1);
            if (c != null) {
                if (c.getCellTypeEnum() == CellType.NUMERIC) {
                    DataFormatter formatter = new DataFormatter();
                    productCode = formatter.formatCellValue(c);
                } else {
                    productCode = c.getStringCellValue();
                }
            }
            p.setProductCode(productCode);

            c = row.getCell(2);
            String processOrder = "";
            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                processOrder = c.getStringCellValue();
            }
            p.setProcessOrder(processOrder);

            c = row.getCell(3);
            double quantity = 0;
            if (c != null && c.getCellTypeEnum() == CellType.NUMERIC) {
                quantity = c.getNumericCellValue();
            }
            p.setQuantity(quantity);

            String comment = "";
            c = row.getCell(4);

            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                comment = c.getStringCellValue();
            }

            p.setComment(comment);

            bulks.add(p);
        }

        workbook.close();
        excelFile.close();
        return bulks;
    }

    public List<WUP> importWUPs(MultipartFile file)  throws IOException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         InputStream excelFile = new BufferedInputStream(file.getInputStream());
        Workbook workbook = new XSSFWorkbook(excelFile);

        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rows = sheet.iterator();

        List<WUP> wups = new ArrayList<WUP>();
        while (rows.hasNext()) {
            Row row = rows.next();
            if (row.getRowNum() == 0) {
                continue;
            }
            WUP p = new WUP();
            Cell c = row.getCell(0);
            String operator = "";
            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                operator = c.getStringCellValue();
            }

            p.setOperator(operator);

         

            String productCode = "";
            c = row.getCell(1);
            if (c != null) {
                if (c.getCellTypeEnum() == CellType.NUMERIC) {
                    DataFormatter formatter = new DataFormatter();
                    productCode = formatter.formatCellValue(c);
                } else {
                    productCode = c.getStringCellValue();
                }
            }
            p.setProductCode(productCode);

            c = row.getCell(2);
            String processOrder = "";
            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                processOrder = c.getStringCellValue();
            }
            p.setProcessOrder(processOrder);

            c = row.getCell(3);
            double quantity = 0;
            if (c != null && c.getCellTypeEnum() == CellType.NUMERIC) {
                quantity = c.getNumericCellValue();
            }
            p.setQuantity(quantity);

            String comment = "";
            c = row.getCell(4);

            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                comment = c.getStringCellValue();
            }

            p.setComment(comment);

            wups.add(p);
        }

        workbook.close();
        excelFile.close();
        return wups;
    }
    
    public List<MixerData> importMixerDatas(MultipartFile file)  throws IOException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("inside import mixer");
         InputStream excelFile = new BufferedInputStream(file.getInputStream());
        Workbook workbook = new XSSFWorkbook(excelFile);

        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rows = sheet.iterator();

        List<MixerData> dataList = new ArrayList<>();
        while (rows.hasNext()) {
            System.out.println("inside row loop");
            Row row = rows.next();
            if (row.getRowNum() == 0) {
                continue;
            }
            MixerData data = new MixerData();
            Cell c = row.getCell(0);

            String groupCode = "";
            if (c != null) {
                if (c.getCellTypeEnum() == CellType.NUMERIC) {
                    DataFormatter formatter = new DataFormatter();
                    groupCode = formatter.formatCellValue(c);
                } else {
                    groupCode = c.getStringCellValue();
                }
            }
            data.setGroupCode(groupCode);

            c = row.getCell(1);
            String processOrder = "";
            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                processOrder = c.getStringCellValue();
            }
            data.setProcessOrder(processOrder);
            
             c = row.getCell(2);
            String description = "";
            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                description = c.getStringCellValue();
            }
            data.setDescription(description);

            c = row.getCell(3);
            double quantity = 0;
            if (c != null && c.getCellTypeEnum() == CellType.NUMERIC) {
                quantity = c.getNumericCellValue();
            }
            data.setQuantity(quantity);

            String startTime = "";
            c = row.getCell(4);
            if (c != null && c.getCellTypeEnum() == CellType.NUMERIC) {
                DataFormatter formatter = new DataFormatter();
                startTime = formatter.formatCellValue(c);
            }
            data.setStartTime(startTime);
            
            String endTime = "";
            c = row.getCell(5);
            if (c != null && c.getCellTypeEnum() == CellType.NUMERIC) {
                DataFormatter formatter = new DataFormatter();
                endTime = formatter.formatCellValue(c);
            }
            data.setEndTime(endTime);
            
            String comment = "";
            c = row.getCell(6);

            if (c != null && c.getCellTypeEnum() == CellType.STRING) {
                comment = c.getStringCellValue();
            }

            data.setComment(comment);

            dataList.add(data);
        }

        workbook.close();
        excelFile.close();
        return dataList;
    }
}
