package com.henchman.henchman.repository;


import com.henchman.henchman.model.Role;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role, Integer>{
	Role findByRole(String role);

    public List<Role> findByRoleNot(String role);

}
