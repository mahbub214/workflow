var chart = {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Time interval for each process order'
    },
    xAxis: {
        categories: [
            '1',
            '2',
            '3',
            '4',
            '5',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Time interval (min)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">Process order: {point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} min</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
            name: 'Time interval',
            data: [49.9, 71.5, 106.4, 129.2, 144.0]

        }]
};
